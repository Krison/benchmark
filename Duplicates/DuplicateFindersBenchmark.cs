﻿using BenchmarkDotNet.Attributes;

namespace Benchmark.Duplicates;

public class DuplicateFindersBenchmark
{
    private static int[] _collection;

    [Params(100, 1000, 10000, 100000)]
    public int Size { get; set; }

    [Params(0.3, 0.6, 0.9)]
    public double DuplicatePosition { get; set; }

    [GlobalSetup]
    public void GlobalSetup()
    {
        _collection = Enumerable.Range(0, Size).ToArray();

        var index = (int)(DuplicatePosition * Size);

        _collection[index] = _collection[index + 1];
    }

    [Benchmark]
    public bool BrutForce()
    {
        return DuplicateFinders.BrutForce(_collection);
    }

    [Benchmark]
    public bool ForEach()
    {
        return DuplicateFinders.ForEach(_collection);
    }

    [Benchmark]
    public bool Any()
    {
        return DuplicateFinders.Any(_collection);
    }

    [Benchmark]
    public bool All() 
    {
        return DuplicateFinders.All(_collection);
    }

    [Benchmark]
    public bool GroupBy()
    {
        return DuplicateFinders.GroupBy(_collection);
    }

    [Benchmark]
    public bool Distinct()
    {
        return DuplicateFinders.Distinct(_collection);
    }

    [Benchmark]
    public bool ToHashSet()
    {
        return DuplicateFinders.ToHashSet(_collection);
    }

    [Benchmark]
    public bool NewHashSet()
    {
        return DuplicateFinders.NewHashSet(_collection);
    }
}