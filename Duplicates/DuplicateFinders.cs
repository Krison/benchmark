﻿namespace Benchmark.Duplicates;

public static class DuplicateFinders
{
    public static bool BrutForce<T>(IEnumerable<T> enumerable)
    {
        int outerIndex = -1;

        foreach (var outerElement in enumerable)
        {
            outerIndex++;

            int innerIndex = -1;
            foreach (var innerElement in enumerable)
            {
                innerIndex++;

                if (outerIndex == innerIndex) continue;

                if (outerElement.Equals(innerElement)) return true;
            }
        }

        return false;
    }

    public static bool ForEach<T>(IEnumerable<T> enumerable)
    {
        HashSet<T> set = new();

        foreach (var element in enumerable)
        {
            if (!set.Add(element))
            {
                return true;
            }
        }

        return false;
    }

    public static bool Any<T>(IEnumerable<T> enumerable)
    {
        HashSet<T> set = new();

        return enumerable.Any(x => !set.Add(x));
    }

    public static bool All<T>(IEnumerable<T> enumerable)
    {
        HashSet<T> set = new();

        return !enumerable.All(set.Add);
    }

    public static bool GroupBy<T>(IEnumerable<T> enumerable)
    {
        return enumerable.GroupBy(x => x).Any(y => y.Count() > 1);
    }

    public static bool Distinct<T>(IEnumerable<T> enumerable)
    {
        return enumerable.Distinct().Count() != enumerable.Count();
    }

    public static bool ToHashSet<T>(IEnumerable<T> enumerable)
    {
        return enumerable.ToHashSet().Count != enumerable.Count();
    }

    public static bool NewHashSet<T>(IEnumerable<T> enumerable)
    {
        return new HashSet<T>(enumerable).Count != enumerable.Count();
    }
}
