﻿namespace Benchmark.IterationStatements;

public class IterationStatements
{
    public static void ForEach<T>(IEnumerable<T> enumerable)
    {
        foreach (var item in enumerable) 
        { 

        }
    }

    public static void For<T>(IEnumerable<T> enumerable) 
    {
        for (int i = 0; i < enumerable.Count(); i++)
        {

        }
    }

    public static void Do<T>(IEnumerable<T> enumerable)
    {
        int n = 0;

        do
        {
            n++;
        } while (n < enumerable.Count());
    }

    public static void While<T>(IEnumerable<T> enumerable)
    {
        int n = 0;
        while (n < enumerable.Count())
        {
            n++;
        }
    }
}
