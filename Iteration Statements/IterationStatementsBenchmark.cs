﻿using BenchmarkDotNet.Attributes;

namespace Benchmark.IterationStatements;

public class IterationStatementsBenchmark
{
    private static int[] _collection;

    [Params(100, 1000, 10000, 100000)]
    public int Size { get; set; }

    [GlobalSetup]
    public void GlobalSetup()
    {
        _collection = Enumerable.Range(0, Size).ToArray();
    }

    [Benchmark]
    public void ForEach()
    {
        IterationStatements.ForEach(_collection);
    }

    [Benchmark]
    public void For()
    {
        IterationStatements.For(_collection);
    }

    [Benchmark]
    public void Do()
    {
        IterationStatements.Do(_collection);
    }

    [Benchmark]
    public void While()
    {
        IterationStatements.While(_collection);
    }
}
