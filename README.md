# Benchmark

##Description

Benchmark project inspired by [Milan Jovanović](https://www.youtube.com/@MilanJovanovicTech).

I will be benchmarking different .Net events, patterns and ideas using [BenchmarkDotNet](https://github.com/dotnet/BenchmarkDotNet#readme).
Code and result will be posted in different subfolder with name corresponding to what is benchmarked.

Feel free to use everything whenever and wherever you want
